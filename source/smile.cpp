#include <emscripten/bind.h>
#include "opencv2/core/core.hpp"
//imread
#include "opencv2/imgcodecs/imgcodecs.hpp"
//cascadeClassifier
#include "opencv2/objdetect/objdetect.hpp"
//resize, ellipse
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace emscripten;
using namespace std;
using namespace cv;

emscripten::val matToObject(cv::Mat mat)
{
    emscripten::val image = emscripten::val::object();
    image.set("data", emscripten::val(emscripten::memory_view<uint8_t>( (mat.total()*mat.elemSize())/sizeof(uint8_t), (uint8_t*) mat.data )));
    image.set("channels", emscripten::val(mat.channels()));
    image.set("channelSize", emscripten::val(mat.elemSize1()));
    image.set("width", emscripten::val(mat.cols));
    image.set("height", emscripten::val(mat.rows));
    return image;
}

cv::Mat imageToMat(const emscripten::val& input)
{
    int w= input["width"].as<unsigned>();
	int h= input["height"].as<unsigned>();
	std::string str = input["data"]["buffer"].as<std::string>();
	const cv::Mat inputMat = cv::Mat(h, w, 24, (void*)str.data(), 0);
	return inputMat;
}

cv::Size minSize(70, 70);
CascadeClassifier cascade, nestedCascade;
double scale = 1;
bool tryflip;

bool loadData()
{
	bool cascadeRes1 = cascade.load("data/haarcascade_frontalface_alt.xml");
	bool cascadeRes2 = nestedCascade.load("data/haarcascade_smile.xml");
	return cascadeRes1 && cascadeRes2;
}

void setMinSize(int width, int height)
{
	minSize = cv::Size(width, height);
}

emscripten::val detect(const emscripten::val& input)
{
    Mat img = imageToMat(input);
    vector<Rect> faces, faces2;
    const static Scalar colors[] =
    {
        Scalar(255,0,0),
        Scalar(255,128,0),
        Scalar(255,255,0),
        Scalar(0,255,0),
        Scalar(0,128,255),
        Scalar(0,255,255),
        Scalar(0,0,255),
        Scalar(255,0,255)
    };
    Mat gray, smallImg;

    cvtColor( img, gray, COLOR_BGR2GRAY );

    double fx = 1 / scale;
    resize( gray, smallImg, Size(), fx, fx, INTER_LINEAR );
    equalizeHist( smallImg, smallImg );

    cascade.detectMultiScale( smallImg, faces,
        1.1, 2, 0
        //|CASCADE_FIND_BIGGEST_OBJECT
        //|CASCADE_DO_ROUGH_SEARCH
        |CASCADE_SCALE_IMAGE,
        minSize );
    if( tryflip )
    {
        flip(smallImg, smallImg, 1);
        cascade.detectMultiScale( smallImg, faces2,
                                 1.1, 2, 0
                                 //|CASCADE_FIND_BIGGEST_OBJECT
                                 //|CASCADE_DO_ROUGH_SEARCH
                                 |CASCADE_SCALE_IMAGE,
                                 minSize );
        for( vector<Rect>::const_iterator r = faces2.begin(); r != faces2.end(); ++r )
        {
            faces.push_back(Rect(smallImg.cols - r->x - r->width, r->y, r->width, r->height));
        }
    }

    for ( size_t i = 0; i < faces.size(); i++ )
    {
        Rect r = faces[i];
        Mat smallImgROI;
        vector<Rect> nestedObjects;
        Point center;
        Scalar color = colors[i%8];
        int radius;

        double aspect_ratio = (double)r.width/r.height;
        if( 0.75 < aspect_ratio && aspect_ratio < 1.3 )
        {
            center.x = cvRound((r.x + r.width*0.5)*scale);
            center.y = cvRound((r.y + r.height*0.5)*scale);
            radius = cvRound((r.width + r.height)*0.25*scale);
            circle( img, center, radius, color, 3, 8, 0 );
        }
        else
            rectangle( img, cvPoint(cvRound(r.x*scale), cvRound(r.y*scale)),
                       cvPoint(cvRound((r.x + r.width-1)*scale), cvRound((r.y + r.height-1)*scale)),
                       color, 3, 8, 0);

        const int half_height=cvRound((float)r.height/2);
        r.y=r.y + half_height;
        r.height = half_height-1;
        smallImgROI = smallImg( r );
        nestedCascade.detectMultiScale( smallImgROI, nestedObjects,
            1.1, 0, 0
            //|CASCADE_FIND_BIGGEST_OBJECT
            //|CASCADE_DO_ROUGH_SEARCH
            //|CASCADE_DO_CANNY_PRUNING
            |CASCADE_SCALE_IMAGE,
            Size(30, 30) );

        // The number of detected neighbors depends on image size (and also illumination, etc.). The
        // following steps use a floating minimum and maximum of neighbors. Intensity thus estimated will be
        //accurate only after a first smile has been displayed by the user.
        const int smile_neighbors = (int)nestedObjects.size();
        static int max_neighbors=-1;
        static int min_neighbors=-1;
        if (min_neighbors == -1) min_neighbors = smile_neighbors;
        max_neighbors = MAX(max_neighbors, smile_neighbors);

        // Draw rectangle on the left side of the image reflecting smile intensity
        float intensityZeroOne = ((float)smile_neighbors - min_neighbors) / (max_neighbors - min_neighbors + 1);
        int rect_height = cvRound((float)img.rows * intensityZeroOne);
        Scalar col = Scalar((float)255 * intensityZeroOne, 0, 0);
        rectangle(img, cvPoint(0, img.rows), cvPoint(img.cols/10, img.rows - rect_height), col, -1);
    }

    return matToObject(img);
}

EMSCRIPTEN_BINDINGS(my_module)
{
    emscripten::function("loadData", &loadData);
    emscripten::function("setMinSize", &setMinSize);
    emscripten::function("detect", &detect);
}
