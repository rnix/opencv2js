#include <emscripten/bind.h>
#include "opencv2/core/core.hpp"
//imread
#include "opencv2/imgcodecs/imgcodecs.hpp"
//cascadeClassifier
#include "opencv2/objdetect/objdetect.hpp"
//resize, ellipse
#include "opencv2/imgproc/imgproc.hpp"


#include <iostream>
#include <stdio.h>

using namespace emscripten;
using namespace std;
using namespace cv;

cv::CascadeClassifier cascadeClassifier;
cv::Mat mask;
cv::Size minSize(100, 100);

bool loadData() {
	bool cascadeRes = cascadeClassifier.load("data/cats.xml");
	mask = imread("data/mask.png", -1);
	if (cascadeRes && mask.data) {
	   return true;
	}
	return false;
}

void setMinSize(int width, int height) {
	minSize = cv::Size(width, height);
}

emscripten::val detectFace(const emscripten::val& input) {
	//convert js img
	int w= input["width"].as<unsigned>();
	int h= input["height"].as<unsigned>();
	std::string str = input["data"]["buffer"].as<std::string>();
	const cv::Mat inputMat = cv::Mat(h, w, 24, (void*)str.data(), 0);
	
	float mult = 1.7;
	float offsetMultWidth = 5.2;
	float offsetMultHeight = 3.0;

	std::vector<Rect> faces;
	cv::Mat frame_gray;
	cv::Mat output;

	cv::cvtColor(inputMat, frame_gray, CV_BGR2GRAY);
	cv::equalizeHist(frame_gray, frame_gray);
	cv::cvtColor(inputMat, output, CV_RGBA2RGB);

	//-- Detect faces
	cascadeClassifier.detectMultiScale(frame_gray, faces, 1.3, 5, 0 | CV_HAAR_SCALE_IMAGE, minSize);

	cv::Mat resizedMask(mask.rows, mask.cols, CV_8UC4);

	for (size_t i = 0; i < faces.size(); i++)
	{
		cv::Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
		cv::ellipse(output, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, cv::Scalar(255, 0, 255), 4, 8, 0);
	
		int newWidth = (int)(faces[i].width * mult);
		int newHeight = (int)(faces[i].height * mult);
		cv::resize(mask, resizedMask, cv::Size(newWidth, newHeight));

		for (int y = 0;  y < resizedMask.rows; y++) {
			for (int x = 0; x < resizedMask.cols; x++) {
				float alphaChannel = resizedMask.at<Vec4b>(y,x)[3] / 255;
				float alpha = alphaChannel;
				int offsetX = -newWidth / offsetMultWidth;
				int offsetY = -newHeight / offsetMultHeight;
				int fx = x + faces[i].x + offsetX;
				int fy = y + faces[i].y + offsetY;
				if (fx >= 0 && fx < output.cols && fy >= 0 && fy < output.rows) {
					output.at<cv::Vec3b>(fy, fx)[0] = (1 - alpha) * output.at<cv::Vec3b>(fy, fx)[0] + (alpha * resizedMask.at<cv::Vec4b>(y, x)[0]);
					output.at<cv::Vec3b>(fy, fx)[1] = (1 - alpha) * output.at<cv::Vec3b>(fy, fx)[1] + (alpha * resizedMask.at<cv::Vec4b>(y, x)[1]);
					output.at<cv::Vec3b>(fy, fx)[2] = (1 - alpha) * output.at<cv::Vec3b>(fy, fx)[2] + (alpha * resizedMask.at<cv::Vec4b>(y, x)[2]);
				}
			}
		}
	}

    return emscripten::val(emscripten::memory_view<uint8_t>( (output.total()*output.elemSize())/sizeof(uint8_t), (uint8_t*) output.data));
}

EMSCRIPTEN_BINDINGS(my_module) {
    emscripten::function("loadData", &loadData);
    emscripten::function("setMinSize", &setMinSize);
    emscripten::function("detectFace", &detectFace);
}
