# opencv2js

An example of building script from c and cpp with OpenCV into javascript using Emscripten in Docker.

# Dependencies

* docker
* git

# Run building

`build.bat` on Windows

`./build.sh` on Linux

# How it works

It downloads OpenCV from Github. Runs emsdk in docker's container.
It builds OpenCV into LLVM byte code. Then it builds code from sources and links it with OpenCV.
It produces js code with Embindings.
Take a look on html/index.html and html/main.js.

The result of building is located in build/some-dir. Put them together with html/* and run index.html.
