@echo off

cd %~dp0
IF NOT EXIST ./opencv git clone https://github.com/opencv/opencv.git && cd opencv && git checkout 3.1.0 && cd ..

setlocal ENABLEDELAYEDEXPANSION
SET PWD=%~dp0
SET PWD=%PWD:C:\=/c/%
SET PWD=%PWD:D:\=/d/%
SET PWD=%PWD:E:\=/E/%
SET PWD=%PWD:\=/%
docker run --rm -t -v %PWD%:/src rnix/emsdk /src/inner_build.sh "%*"
pause
