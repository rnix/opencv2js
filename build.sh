DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"
if [ ! -d "$DIR" ]; then
  git clone https://github.com/opencv/opencv.git && cd opencv && git checkout 3.1.0 && cd ..
fi
 
docker run --rm -t -v "$DIR":/src rnix/emsdk /src/inner_build.sh "%*"
