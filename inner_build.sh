#!/bin/bash

###INCLUDES="-I../opencv/modules/core/include -I../opencv/modules/ts/include -I../opencv/modules/highgui/include -I../opencv/modules/videoio/include -I../opencv/modules/flann/include -I../opencv/modules/ml/include -I../opencv/modules/photo/include -I../opencv/modules/shape/include -I../opencv/modules/hal/include -I../opencv/modules/video/include -I../opencv/modules/features2d/include -I../opencv/modules/calib3d/include -I../opencv/modules/objdetect/include -I../opencv/modules/imgproc/include -I../opencv/modules/imgcodecs/include"
###SOURCE_FILES="./cats.cpp"
###LIBS="../opencv/build/lib/libopencv_objdetect.a ../opencv/build/lib/libopencv_imgcodecs.a ../opencv/build/lib/libopencv_imgproc.a ../opencv/build/lib/libopencv_core.a ../opencv/build/3rdparty/lib/liblibpng.a ../opencv/build/3rdparty/lib/libzlib.a"
###MODULE_NAME="cats"

# Load above mentioned variables
if [ ! -z "$1" ]; then
  PROJECT="$1";
else
  PROJECT=smile
fi

CONF_FILE="./source/$PROJECT.ini"

if [ ! -f "$CONF_FILE" ]; then
  echo "CONF_FILE=$CONF_FILE can not be loaded."
  exit 1
fi

source "$CONF_FILE"

if [ -z "$SOURCE_FILES" ];  then
  echo "Variable SOURCE_FILES is not set."
  exit 1
fi

if [ -z "$MODULE_NAME" ];  then
  echo "Variable MODULE_NAME is not set."
  exit 1
fi


if [ ! -d ./opencv ]; then
  echo "Download OpenCV first"
  exit 1
fi

echo "Building $MODULE_NAME"

mkdir -p opencv/build
cd opencv/build

FLAGS="-O3 --llvm-lto 1"

if [ ! -f lib/libopencv_core.a ]; then
emcmake cmake \
    -DCMAKE_BUILD_TYPE=RELEASE \
    -DBUILD_DOCS=OFF \
	-DBUILD_EXAMPLES=OFF \
	-DBUILD_PACKAGE=OFF \
	-DBUILD_WITH_DEBUG_INFO=OFF \
	-DBUILD_opencv_bioinspired=OFF \
	-DBUILD_opencv_calib3d=OFF \
	-DBUILD_opencv_cuda=OFF \
	-DBUILD_opencv_cudaarithm=OFF \
	-DBUILD_opencv_cudabgsegm=OFF \
	-DBUILD_opencv_cudacodec=OFF \
	-DBUILD_opencv_cudafeatures2d=OFF \
	-DBUILD_opencv_cudafilters=OFF \
	-DBUILD_opencv_cudaimgproc=OFF \
	-DBUILD_opencv_cudaoptflow=OFF \
	-DBUILD_opencv_cudastereo=OFF \
	-DBUILD_opencv_cudawarping=OFF \
	-DBUILD_opencv_gpu=OFF \
	-DBUILD_opencv_gpuarithm=OFF \
	-DBUILD_opencv_gpubgsegm=OFF \
	-DBUILD_opencv_gpucodec=OFF \
	-DBUILD_opencv_gpufeatures2d=OFF \
	-DBUILD_opencv_gpufilters=OFF \
	-DBUILD_opencv_gpuimgproc=OFF \
	-DBUILD_opencv_gpuoptflow=OFF \
	-DBUILD_opencv_gpustereo=OFF \
	-DBUILD_opencv_gpuwarping=OFF \
	-BUILD_opencv_hal=OFF \
	-DBUILD_opencv_highgui=ON \
	-DBUILD_opencv_java=OFF \
	-DBUILD_opencv_legacy=OFF \
	-DBUILD_opencv_ml=ON \
	-DBUILD_opencv_nonfree=OFF \
	-DBUILD_opencv_optim=OFF \
	-DBUILD_opencv_photo=ON \
	-DBUILD_opencv_shape=ON \
	-DBUILD_opencv_objdetect=ON \
	-DBUILD_opencv_softcascade=ON \
	-DBUILD_opencv_stitching=OFF \
	-DBUILD_opencv_superres=OFF \
	-DBUILD_opencv_ts=OFF \
	-DBUILD_opencv_videostab=OFF \
	-DWITH_1394=OFF \
	-DWITH_CUDA=OFF \
	-DWITH_CUFFT=OFF \
	-DWITH_EIGEN=OFF \
	-DWITH_FFMPEG=OFF \
	-DWITH_GIGEAPI=OFF \
	-DWITH_GSTREAMER=OFF \
	-DWITH_GTK=OFF \
	-DWITH_JASPER=OFF \
	-DWITH_JPEG=OFF \
	-DWITH_OPENCL=OFF \
	-DWITH_OPENCLAMDBLAS=OFF \
	-DWITH_OPENCLAMDFFT=OFF \
	-DWITH_OPENEXR=OFF \
	-DWITH_PNG=ON \
	-DWITH_PVAPI=OFF \
	-DWITH_TIFF=OFF \
	-DWITH_LIBV4L=OFF \
	-DWITH_WEBP=OFF \
	-DWITH_PTHREADS_PF=OFF \
	-DBUILD_opencv_apps=OFF \
	-DBUILD_PERF_TESTS=OFF \
	-DBUILD_TESTS=OFF \
	-DBUILD_SHARED_LIBS=OFF \
	-DWITH_IPP=OFF \
	-DENABLE_SSE=OFF \
	-DENABLE_SSE2=OFF \
	-DENABLE_SSE3=OFF \
	-DENABLE_SSE41=OFF \
	-DENABLE_SSE42=OFF \
	-DENABLE_AVX=OFF \
	-DENABLE_AVX2=OFF \
   -DCMAKE_CXX_FLAGS="$FLAGS" \
   -DCMAKE_EXE_LINKER_FLAGS="$FLAGS" \
   -DCMAKE_CXX_FLAGS_DEBUG="$FLAGS" \
   -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="$FLAGS" \
   -DCMAKE_C_FLAGS_RELWITHDEBINFO="$FLAGS" \
   -DCMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO="$FLAGS" \
   -DCMAKE_MODULE_LINKER_FLAGS_RELEASE="$FLAGS" \
   -DCMAKE_MODULE_LINKER_FLAGS_DEBUG="$FLAGS" \
   -DCMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO="$FLAGS" \
   -DCMAKE_SHARED_LINKER_FLAGS_RELEASE="$FLAGS" \
   -DCMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO="$FLAGS" \
   -DCMAKE_SHARED_LINKER_FLAGS_DEBUG="$FLAGS" \
   ..

  emmake make -j4
fi

mkdir -p ../../build
cd ../../source

echo "Compiling main code into bc"

FLAGS="$FLAGS -s TOTAL_MEMORY=268435456"

emcc --bind $SOURCE_FILES $FLAGS $INCLUDES -o "../build/$MODULE_NAME.bc"
if [ ! -f "../build/$MODULE_NAME.bc" ]; then
  echo "Compiling main code into bc - FAIL"
  exit 1
fi

echo "Linking with OpenCV"
emcc "../build/$MODULE_NAME.bc" $LIBS -o ../build/libModuleOpenCV.bc
if [ ! -f ../build/libModuleOpenCV.bc ]; then
  echo "Linking with OpenCV - FAIL"
  exit 1
fi

echo "Final building js"
mkdir -p "../build/$MODULE_NAME"
emcc --bind ../build/libModuleOpenCV.bc $FLAGS --preload-file data/ -o "../build/$MODULE_NAME/$MODULE_NAME.js"
if [ ! -f "../build/$MODULE_NAME/$MODULE_NAME.js" ]; then
  echo "Final building js - FAIL"
  exit 1
fi

cd ..
echo "Done: build/$MODULE_NAME/$MODULE_NAME.js"
